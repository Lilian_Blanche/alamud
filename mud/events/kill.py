from .event import EventJJ
class KillWithEvent(EventJJ):
    NAME = "kill-with"

    def perform(self):
        self.inform("kill-with")
